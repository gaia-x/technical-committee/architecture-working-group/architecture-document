# Decentralized Digital (Data) Ecosystems

Digital Ecosystems follow common organizational and technical design patterns as e.g. described in the Building Blocks of the [Data Spaces Support Centre](https://dssc.eu).

![DSSC Building Blocks v 1.5](docs/figures/DSSC_Building_Blocks.png)

Different ecosystems will choose the specific implementation of a building block based on criteria related for example to:

- local regulations
- domain-specific requirements (e.g. referring to Data Model standards or technical standards)
- existing legal and commercial agreements
- membership in the ecosystem
- relevant legal frameworks
- Service Level Agreements
- access to marketplaces

## Common Threads

To ensure that participants from different backgrounds can start to interact with each other it is important to agree on a common semantics for the description of the ecosystem components; the globally adopted standard to describe decentralized systems is governed by the W3C. It provides:

* RDF (Resource Description Framework) as semantic standard
  * including: RDF Schema as vocabulary, SPARQL as Data Constraint Language, Turtle and JSON-LD
* DID (Decentralized Identifiers)
* VC (Verifiable Credentials)
* DCAT (Data Catalogue Vocabularies)
* ODRL as Rights Definiton Language

Many specific existing standards can be mapped to W3C and expressed in a way that equivalence between two standards can be described.

## Gaia-X as basis of interoperable ecosystems

Gaia-X provides a model where different standards, mainly from W3C, are combined, with the aim to operationalise and automate to the maximum extent possible the ecosystem governance, in a sovereign and secure manner.

### Conformity Framework and Ecosystem Registry

![Gaia-X Conformity Framework](docs/figures/Gaia-X_Conformity_Schema.png)

Gaia-X provides the mechanism for an ecosystem to translate the individual ecosystem agreements into an RDF schema and to an associated list of trust anchors which are able to make a machine-readable attestation ("issue a Verifiable Credential") which then can be evaluated by the other participants.

![Gaia-X Registry](docs/figures/D3E_Registry.jpg)

### Verification framework for ecosystem enabling services

This "Ecosystem Registry" can be used to validate all the operators which are supporting enabling services for the ecosystem. Usually, the validation includes legal and regulatory compliance, and defined commercial agreements including service level agreements:

![Gaia-X Enabling Services](docs/figures/D3E_Enabling_Services.png
)
Especially:

### Validation and Verification Services

This allows each ecosystem to define those parties who can issue and verify conformity credentials for the criteria defined following the ecosystem Policy Rules. Using the Trust Anchor Credentials defined in the Architecture and ICAM documents this function can be delegated.

### Policy Information Points

Policy Information Points evaluate a specific set of rules and can be used as input to both the Validation and Verification services as well as any policy-based reasoning (e.g. by the Dataspace Protocol).

### Federated Catalogue Services

In order to discover the agreed references (e.g. Vocabularies, Data Models,...) as well as to publish available services the ecosystem often provides a controlled publication by trusted operators

### Auditing / Observability Services

### Portals

## Data Spaces Protocol

Data Spaces are a specific kind of ecosystem. The Dataspace Protocol defines the mechanism on how two participants individually negotiate the exchange of data.

![Dataspace Protocol](docs/figures/D3E_DSP.png)

The Conformity and Verification Framework ensures compliance to the policy rules set by the ecosystem governance

* building block services and their operation
* participant identity and participant services
* any claims made by the participants can be validated and verified

## Ecosystem Interoperability

![D3E Interoperability](docs/figures/D3E_Interoperability.png)

The principles above base participation in an ecosystem on compliance to the defined Policy Rules. Any participant who complies with the set of rules (and validated through the system of trust anchors) can participate in a given ecosystem. Ecosystems can trust each other's operators and validation and verification mechanisms.
