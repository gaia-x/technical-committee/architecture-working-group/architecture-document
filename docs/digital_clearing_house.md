# Gaia-X Digital Clearing House (GXDCH)

The Gaia-X Digital Clearing House (GXDCH) operationalizes the Gaia-X mission.
A GXDCH makes the various mechanisms and concepts applicable in practice as a ready-to-use service set.
Therefore, a distinct specification and implementation of the GXDCH exists.

The [Gaia-X Framework](http://docs.gaia-x.eu/framework/) contains functional and technical specifications, the Gaia-X Compliance Service to assess Gaia-X Compliance and the [testbed](software_framework.md#testbed) to validate the behaviour of software components against the Gaia-X Policy Rules and technical specifications.

The GXDCH contains both mandatory and optional components. 

All the mandatory components of the GXDCH are open-source software.
The development and architecture of the GXDCH is under the governance of the Gaia-X Association.

A GXDCH instance runs the engine to validate the Gaia-X rules, therefore becoming the go-to place to become Gaia-X compliant.
The instances are non-exclusive, interchangeable, and operated by multiple market operators.  

!!! note

    It is crucial to differentiate **compliance** and **compatibility**.  
    A service can be made Gaia-X compliant with Gaia-X Policy Rules (see the Policy Rules Compliance Document). A software cannot.  
    However, a software can be made Gaia-X compatible with Gaia-X specifications.

## Deployment 

Each GXDCH instance must be operated by a service provider according to rules defined with and approved by the Gaia-X Association.

Such providers have then the role of [Operators](conceptual_model.md).
Gaia-X is not an operator itself.

Any provider compliant to the requirements defined by the Gaia-X Association and featuring the necessary characteristics as defined by Gaia-X can become a GXDCH operator.

## Description of the GXDCH components

The GXDCH is open for future evolution. This means that the components included in the GXDCH may be enhanced from release to release.

### Mandatory components

The components below are:

- technically compatible with the Gaia-X Specifications described in the functional and technical Specifications Documents of the [Gaia-X Framework](https://docs.gaia-x.eu/framework/)
- validated by the [Gaia-X Testbed](#testbed), which will aim to verify the behaviour of the components.

Thoses services are provided by GXDCH operators only.

* the Gaia-X Compliance Service  
It validates the shape and content of the Gaia-X Credentials and, if successful, issues a `GaiaXComplianceCredential`.
* the Gaia-X Registry  
This service implements the <!-- langec: a link in Markdown syntax would be more maintainable -->[Gaia-X Registry](https://gaia-x.gitlab.io/technical-committee/architecture-document/operating_model/#gaia-x-registry) and presents the backbone of the Gaia-X Ecosystem governance.  
It stores trust anchors accepted by the <!-- langec: use Markdown for link -->[Gaia-X Trust Framework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/), the shapes, and schemas that are used for the Gaia-X Compliance validation. Beyond these core functionalities, the Gaia-X Registry Service stores further accompanying information such as the terms and conditions for the Gaia-X Compliance Service.
* the Gaia-X Notarisation Services
  * for the business registration numbers: this is a tool used to verify all registration numbers given by the participant in their Participant credentials.
* the Credential Event Service  
Provides a distributed storing solution to hold Gaia-X Compliant VCs and is used to synchronise the Credentials' IDs across the [Federated Catalogues](federated_catalogue.md)<!-- langec: This section no longer exists as a separate file. --> that want to publish Gaia-X compliant services.
* a [InterPlanetary File System (IPFS)](https://www.ipfs.tech/) node  
This is used to synchronise the information across GXDCH instances, especially the information exposed by the Gaia-X Registry.
* a Logging service  
This service is not necessarily exposed to customers. This is subject to the operator depending on the operator's applicable regulations.

### Optional components

The following services are optional and can be provided by any Providers, which may or may not be a GXDCH operator.
Those services can also be operated by providers for specific dataspaces or federations.

* a Wizard  
UI to sign Verifiable Credentials in a Verifiable Presentation on client side.
Calling the Gaia-X Compliance Service is integrated with the Wizard, making it possible to obtain Gaia-X Compliant Credentials by directly using this tool.
Depending of the implementation, a wizard can support signing with hardware module via [PKCS#11](https://en.wikipedia.org/wiki/PKCS_11) or other like [Web eID](https://web-eid.eu/).
The Wizard also provides a user-friendly way to send the Gaia-X Compliant VCs to the Credential Events Service.
Finally, it provides an environment where the user can try out the Gaia-X Compliance for testing and learning purposes.
* a Wallet  
Application that allows the user to store their Credentials and present them to third parties when needed.
This is also sometimes called an Agent, CredentialRegistry or [IdentityHub](https://identity.foundation/decentralized-web-node/spec/0.0.1-predraft/).


* a Catalogue  
An instance of the Federated Catalogue using the Credential Event Service introduced in the previous section.
* a [Key Management System](https://en.wikipedia.org/wiki/Key_management#Key_management_system)  
This is provided by a provider as an additional service to help its customers to manage cryptographic material, including revocation, key rotation, key recovering, …
* Policy Decision Point (PDP)  
This service is used to perform the reasoning given one or more policy and input data.
The PDP can support several policy languages.
For each policy expressed in a Gaia-X credential, the policy owner can give the list of PDP service endpoints, which can be used to compute the policies.
* Data Exchange services  
Those services are used to perform [Data Exchange](data_exchange_services.md)

The optional services provided may be subject to fees set by the provider.

## Gaia-X Digital Clearing House Releases

Each GXDCH release is a bundle of several components developed by the Gaia-X Lab with the support and contribution of the open-source community.

The components and their documentation are published for Operational and Lab versions at https://gitlab.com/gaia-x/lab/gxdch

### Elbe release

This was an internal release only for testing purposes.

### Tagus release

This is the ongoing development cycle under `/v1`.

In order to allow several instances of different version to be accessible in parallel, we specify paths during the deployment.  
That means that `/development` uses the latest `development` image of a component, `/v1` uses the latest `v1` image, and `/main` uses the latest `main` image.

To ensure consistency, GXDCH operators are requested to use the same convention for their instances.

### Loire release

This is the next major release developed by the Gaia-X Lab with the contribution of the open-source community.

## Gaia-X Digital Clearing House Operations

## GXDCH usage

For the Tagus release, the Gaia-X Association provides a load-balancer in the form of some HAProxy nodes pointing to Clearing House instances.
<!-- langec: quite a lot of technical detail for an Architecture Document -->These HAProxy nodes are managed through [Load Balancer Ansible scripts](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/loadbalancer/README.md). 

The Gaia-X Association is responsible for these scripts and their update after a Clearing House is deployed.
GXDCH operators can open merge requests on the [HAProxy configuration](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/loadbalancer/haproxy.cfg) to include their GXDCH instance.

The services are accessible behind three subdomains:

- https://registrationnumber.notary.gaia-x.eu/v1/docs/
- https://compliance.gaia-x.eu/v1/docs/
- https://registry.gaia-x.eu/v1/docs/

## GXDCH setup

Technical details are available in the [setup guide](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/README.md).




