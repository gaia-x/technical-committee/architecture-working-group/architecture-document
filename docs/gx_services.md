# Gaia-X Trust Framework components

This section focuses on the mandatory software components that are being operated only by the Gaia-X Digital Clearing Houses (GXDCHs) to enable the issuing of Gaia-X Compliance Credentials.

**Note: The sections on Protocol/Interface Spec need to be completed/corrected!**

## Gaia-X Registry

The Gaia-X Registry (see <https://registry.gaia-x.eu>) is a public distributed, non-repudiable, immutable, permissionless database with a decentralised infrastructure and the capacity to automate code execution.

:information_source: The Ecosystems may want to have their own instance of a local Registry or equivalent. Technically, this component can be part of the ecosystem's local Catalogues.

The Gaia-X Registry is the backbone of the ecosystem governance, which stores information, similarly to the [Official Journal of the European Union](https://eur-lex.europa.eu/oj/direct-access.html), such as:

- the list of the Trust Anchors – keyring.
- the result of the Trust Anchors validation processes.
- the potential revocation of Trust Anchors's identity.
- the vote and results of the Gaia-X Association roll call vote, similar to the rules of the [plenary of the European Parliament](https://www.europarl.europa.eu/about-parliament/en/organisation-and-rules/how-plenary-works).
- the shapes and schemas for the Gaia-X VCs.
- the URLs of Gaia-X Catalogue's credentials.
- the text of the Terms and Conditions for Gaia-X Conformity.
- …

The Gaia-X Registry also facilitates the provision of:

1. A decentralized network with smart contract functionality.
2. Voting mechanisms that ensure integrity, non-repudiation, and confidentiality.
3. Access to a Gaia-X Compliance Service instance.
4. A fully operational, decentralized and easily searchable catalogue[^OP].
5. A list of Participants' identities and credentials' URIs which violate Gaia-X membership rules. This list must be used by all Gaia-X Trusted Catalogue providers to filter out any inappropriate content.
6. Tokens may cover the operating cost of the Gaia-X Ecosystem. This specific point can be abstracted by 3rd party brokers wrapping token usage with fiat currency, providing opportunities for new services to be created by the Participants. Emitting tokens for the Gaia-X Association's members is also considered.

The Gaia-X Registry is used by the Gaia-X Compliance Engine to perform the checks needed to assess Gaia-X Compliance and can be used by 3rd parties to get correct information (about the shapes, T&C, etc).
The Gaia-X Registry will be used as the seeding list for the network of Catalogues.

This model enables the Participants to operate in the Gaia-X Ecosystem, to autonomously register information, and to access the information which is verifiable by other Participants.

[^OP]: Example of decentralized data and algorithms marketplace <https://oceanprotocol.com/>

The Gaia-X Registry leverages a combination of DNS, DNSSEC, and IPFS to ensure the integrity, availability, and non-repudiability of stored data. This approach not only secures the data but also makes it globally accessible and resistant to tampering.
Data stored in IPFS includes essential governance documents, shapes, schemas, and other critical ecosystem information. Each piece of data or file added to IPFS is identified by a unique content identifier (CID), which is a cryptographic hash of the data's content.

### DNS TXT Records and Naming Convention

To facilitate the discovery and accessibility of the stored data, the Gaia-X Registry utilizes DNS TXT records. These records are used to advertise the current URIs (in the case of the Gaia-X ecosystem, they are ipfs:// links which include CIDs) associated with the latest versions of the stored documents. The structure of these DNS TXT records follows a specific naming convention to ensure easy and systematic access:

DNS TXT records are formatted as follows:
**`[gxdch-version]._[ontology-version]._[type].[?subdomain.domain]`**

Where:
- **`[gxdch-version]`** Indicates the version of the Clearing House, formatted as (e.g., `vmajor.minor`, `vmajor`).
- **`[ontology-version]`** Indicates the version or codename of the ontology (e.g., `2404`, `danube`).
- **`[type]`** Specifies the type of content, such as `shapes`, `scheme`, or `trust_anchors`.
- **`[domain]`** Represents the domain and optional subdomain where the records are hosted. For Gaia-X the domain is `gxdch.eu`. Other ecosystems can adopt the same design.
- 
Here is a table illustrating the DNS TXT records format for Gaia-X:

| Record Name                        | Record Type | TTL  | Value              |
|------------------------------------|-------------|------|--------------------|
| `v2._2404._shapes.gxdch.eu`        | TXT         | 600  | `ipfs://[CID]`     |
| `v2._2404._scheme.gxdch.eu`        | TXT         | 600  | `ipfs://[CID]`     |
| `v2._2404._trust_anchors.gxdch.eu` | TXT         | 600  | `ipfs://[CID]`     |
| `v2._2404.gxdch.eu`                | TXT         | 600  | `ipfs://[CID]`     |

Each TXT record contains a URI that points to the IPFS location (where `CID` is the Content Identifier for the respective document(s) or data stored on IPFS) or an external URL where the data can be accessed. 
This flexible system allows the registry to use `ipfs://` for decentralized storage or standard `https://` URLs for more traditional data hosting.

If you omit the `[type]`, you will obtain the ipfs URI pointing to the root folder containing all documents available for the given `[version]`.

### Adopting the Gaia-X Model in Other Ecosystems

The Gaia-X Registry's approach to data management and distribution is adaptable. Other ecosystems can implement a similar strategy by setting up their DNS to point to the relevant data URIs. For example, an ecosystem could use a similar DNS TXT record strategy to point to different types of content:

| Record Name                          | Record Type | TTL  | Value                           |
|--------------------------------------|-------------|------|---------------------------------|
| `v1._2404._scheme.your-ecosystem.eu` | TXT         | 600  | `https://yourapi.com/v1/scheme` |


By adopting this model, ecosystems can ensure that their data is easily accessible, verifiable, and secure. The use of versioned records also helps manage updates and maintain compatibility across different versions of the ecosystem's services.
Participants and ecosystems within the Gaia-X ecosystem can query these DNS TXT records to retrieve the latest URIs and ensure that they access the most current and verifiable versions of the data, fostering a trustworthy and transparent ecosystem.
This integration not only enhances the resilience and efficiency of data distribution within the Gaia-X Ecosystem but also aligns with the broader goals of decentralization and robustness in data handling.

### Basic Protocol and Interface Specification for GXDCH Registry

| | |
|-|-|
| Standards used: | W3C:RDF, JSON-LD, OWL, SHACL, SPARQL |
| Protocol: |      Rest-API |
| API: | EBSI, OpenAPI via Swagger @ registry.gaia-x.eu |
| Trust Anchors: | ETSI TS 119 612 |
|||

## Gaia-X Compliance

The service takes as input the Verifiable Presentations provided by the participants, checks them against the SHACL Shapes available in the Registry and performs other consistency checks based on the Gaia-X Policy Rules.

The service returns a Verifiable Credential, the "Gaia-X Compliance Credential" with a Gaia-X signature, as a proof that the input provided has passed all the verifications.

### Basic Protocol and Interface Specification for GXDCH Compliance

| | |
|-|-|
| Standards used: | W3C:RDF, W3C:VC, W3C:DID [DID:WEB] JSON-LD, SHACL Shapes; OIDC4VP; OIDC4VCI |
| Protocol: |      Rest-API |
| API: | OpenAPI via Swagger @ registry.gaia-x.eu |
|||

## Gaia-X Notary - LRN (Legal Registration Number)

- Takes as input a LegalRegistrationNumber VC from the user
- Verifies that the VC contains at least one identification number as requested by the Gaia-X rules, and checks that the number is valid
- Returns a Gaia-X VC with the proof that the number has been verified

### Basic Protocol and Interface Specification for GXDCH Notary

| | |
|-|-|
| Standards used: | W3C RDF, JSON-LD, SHACL Shapes |
| Protocol: |      Rest-API |
| API: | OpenAPI via Swagger @ registry.gaia-x.eu |
|||

## Gaia-X Credential Event Service (CES)

For the Gaia-X Catalogues of the GXDCH to be notified about new, updated, revoked credentials, a common Publication / Subscription service (pubsub service) must be deployed via decentralized [GXDCH](https://gaia-x.eu/gxdch/) instances. This service is called "Credential Event Service" (CES) Its Technical Specification is available in the [Software Specification of the Gaia-X Lab](https://gitlab.com/gaia-x/lab/credentials-events-service)

It is expected that the pubsub service will have different implementations over time from a distributed service during the pilot phase - Apache Kafka or similar - to a decentralized one - a Gaia-X consortium blockchain like for the Gaia-X Registry.

The deployed solutions have to accommodate for convenience from a user point of view and for security from an operator point of view.

- The service only accepts push/pull requests from participants which have a valid Gaia-X Participant Credential to ensure that Terms & Conditions of the service have been agreed. Abuse of the service can lead to revocation of the right to use the CES service.

- The service should provide synchronization within a defined time limit specified in the GXDCH Operations Handbook (tbp) to ensure consistency for users across GXDCH providers.

- Potential handling of downtimes of the CES are specfied in the GXDCH Operations Handbook (tbp). To allow users to mitigate potential lazy syncronization a timestamp for the latest synchchronization must be provided and pointers to potential alternative catalogues should be provided. The design principle follows the [CAP Theorem](https://en.wikipedia.org/wiki/CAP_theorem) which basicly states that the service should be available and partition tolerant but not necessarily consistent.

- The CES is limited to push/pull Gaia-X Compliance credentials only. To avoid publication of confidential data only the credential ID is going to be published. The subscribers of the pubsub services have to resolve the ID and request access to the holder for the credential. The service storing the credential - agent or wallet - is from a functional point of view a Policy Decision Point leaving full control of the credential access to the holder.

- Identification of catalogue providers consuming the data in some way.

- "Log events" on participants consuming data shall be provided to avoid and track abuse.

- The service is going to accept standard OIDC4VP API.

### Basic Protocol and Interface Specification for GXDCH CES

| | |
|-|-|
| Standards used: | |
| Protocol: |      Rest-API |
| API: | OpenAPI via Swagger @ registry.gaia-x.eu, OIDC4VP |
|||

## Graphical overview of the Trust Framework components

![Trust Framework](figures/Gaia-X_Trust_Framework_components_rev.jpg)
*Figure 6.1 - Gaia-X Trust Framework Components*

A complete software architecture is available and maintained on [GitLab](https://gitlab.com/gaia-x/lab/gxdch/-/tree/main/architecture?ref_type=heads). It represents the current software architecture of the GXDCH components, as well as the previous versions and the upcoming ones. 
It's using the renowned [C4 Model](https://c4model.com/).

Note: see the [Annex](annex.md) chapter for the full list of GXDCH components.
