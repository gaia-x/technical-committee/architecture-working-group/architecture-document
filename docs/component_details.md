# Component details

## Resources and Service Offerings

Resources describe in general the goods and objects of the Gaia-X Ecosystem.

*Resource Categories*

A Resource can be a:

- Physical Resource: it has a weight, a position in space and represents a physical entity that hosts, manipulates, or interacts with other physical entities.
- Virtual Resource: static data in any form and necessary information such as a dataset, configuration file, license, keypair, an AI model, neural network weights, …
- Instantiated Virtual Resource: an instance of a Virtual Resource.
It is equivalent to a Service Instance and is characterized by endpoints and access rights.

A Service Offering is a set of Resources, which a Provider aggregates and publishes as a single entry in a Catalogue.

A `Service Offering` can be associated with other `Service Offerings`.

```mermaid
classDiagram
    ServiceOffering o-- Resource: aggregation of
    Resource o-- Resource: aggregation of

class Resource{
    <<abstract>>
}

    Resource <|-- "subclass of" PhysicalResource: 
    Resource <|-- "subclass of" VirtualResource
    VirtualResource <|-- "subclass of" InstantiatedVirtualResource

    InstantiatedVirtualResource <..> ServiceInstance : equivalent to
```
## Policies

### Policy definition

Policy is defined as a statement of objectives, rules, practices, or regulations governing the activities of Participants within Gaia-X.
From a technical perspective, Policies are statements, rules or assertions that specify the correct or expected behaviour of an entity[^9][^10].

The [Gaia-X Policy Rules Compliance Document](https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/) defines the Gaia-X Policies for Service Offerings.
They cover, for example, privacy or cybersecurity policies and are expressed in the Conceptual Model indirectly as attributes of the Resources, Service Offerings, and Service Instances.

### Policy description

The general Policies defined by Gaia-X form the basis for detailed Policies for a particular Service Offering, which can be defined additionally and contain particular restrictions and obligations defined by the respective Provider or Consumer.
They occur either as a Provider Policy (alias Usage Policies) or as a Consumer Policy (alias Search Policy):

- A Provider Policy/Usage Policy constrains the Consumer's use of a Resource.
*For example, a Usage Policy for data can constrain the use of the data by allowing to use it only for _x_ times or for _y_ days.*
- A Consumer Policy describes a Consumer's restrictions on a requested Resource.
*For example, a Consumer gives the restriction that a Provider of a certain service has to fulfil demands such as being located in a particular jurisdiction or fulfilling a certain service level.*

In the Conceptual Model, they appear as attributes in all elements related to Resources.
The specific Policies have to be in line with the general Policies defined by Gaia-X.

[^9]: Singhal, A., Winograd, T., & Scarfone, K. A. (2007). Guide to secure web services: Guide to Secure Web Services - Recommendations of the National Institute of Standards and Technology. Gaithersburg, MD. NIST. <https://csrc.nist.gov/publications/detail/sp/800-95/final> <https://doi.org/10.6028/NIST.SP.800-95>
[^10]: Oldehoeft, A. E. (1992). Foundations of a security policy for use of the National Research and Educational Network. Gaithersburg, MD. NIST. <https://doi.org/10.6028/NIST.IR.4734>

```mermaid
graph TD;
    A[Policy Rules] --> |defines| B[general Gaia-X Policies]
    B --> |basis for| C[Resource-specific Policies]
    C -->D[Provider Policy/ Usage Policy]
    C -->E[Consumer Policy/ Search Policy]
```

#### Policy enforcement

Policy enforcement is executed by software engines (e.g. the Gaia-X Compliance Service or engines within a Data Exchange Service).

## Service composition

### Assumptions

The generic Gaia-X service composition model is derived assuming the availability of key related functions and systems within the Gaia-X Ecosystem.
The first assumption is the availability of a Catalogue containing service offerings compliant with Gaia-X.
These correspond to provider services with self-attested credentials and credentials verified by the Gaia-X Compliance Service.
In addition to these services, there can be external and independent services offered by multiple third parties.
A requirement on these third-party service offerings is to provide independent services compatible with Gaia-X compliant services.
It is thus possible to combine them to produce composite services.
Means to check for compatibility and composability are provided and potentially built into the respective credentials.
Service composition consequently assumes that searching for compatible and interoperable services in multiple Catalogues is possible.
Whenever such capability is not available, service offerings nevertheless come along with service templates and service descriptors and descriptions that readily embed this key information.
Hence, credentials have to contain key characteristics and must have the appropriate structure.
They would embed the following TOSCA-like or similar descriptors:

- Capabilities
- Requirements
- Properties
- Operations
- Artifacts
- Compatibility, interoperability, composability, substitutability attributes (information, list, possibly revealing if service components are bundles)
- Interfaces

Furthermore, we assume this embedded additional information in service descriptions in the Gaia-X service catalog. That is, providing information on how to chain services to ensure successful and failure free composition as well as guaranteed operation at instantiation and run time. The service composition model shall also allow for the portability of applications among cloud Providers, adhering to the high-level objectives described in the [Franco German position on Gaia-X](https://www.google.com/url?sa=t&source=web&rct=j&opi=89978449&url=https://www.bmwk.de/Redaktion/DE/Downloads/F/franco-german-position-on-gaia-x.pdf%3F__blob%3DpublicationFile%26v%3D4&ved=2ahUKEwjL6br444CGAxXCUqQEHat_DWUQFnoECBEQAw&usg=AOvVaw1L_3_GvlhA_xqjKH8gjQhO).

### Generic Service Composition Model

Considering an open European and international context involving multiple stakeholders and participants, the Gaia-X service composition model has to be abstract at the start of any initial service composition.
The focus of the composition model is consequently on the service functional behaviour, with no constraints, localization, or preset values of non-functional attributes.
Values are set only once the end users, tenants or consumers, have expressed their requirements, constraints and preferences.
These values are gradually set as service composition moves from initial provisioning to the life cycle management of services at run time.
Figure 1 depicts a high-level class diagram view of the service composition model.

![Service Composition Model](figures/Fig1_Abstract-High-level_Generic-SC-Model.png)
*Figure 4.1 - Simplified and abstract conceptual service composition model*

This process shall by no means prevent or hamper interoperability, portability, or substitution of services and applications.
This should require no or only minor changes.
Figure 4.1 depicts the abstract and simplified service composition model.
Beyond this simplified and abstract service composition view and class diagram, Figure 4.2 illustrates a more elaborate class diagram representation of the service composition model.
The figure includes details and related functions and modules involved in service composition starting from a user service request to the instantiation of the services by providers.
It also includes and extends the Gaia-X conceptual model with an expanded view of the service composition model.

### Conceptual Service Composition Model

We start with a generic and abstract view of the conceptual service composition model and illustrate where it fits in the Gaia-X conceptual model.
As far as the Gaia-X service composition model is concerned, both API and template-based services have to be included in the modelling framework.
The role "Operator" in the diagram below indicates a provider which has been designated by the ecosystem governance to operate "Federation Services"

![Service Composition](figures/Fig_4.2_Service_Composition_rev2.drawio.png)
*Figure 4.2 - Gaia-X Conceptual Architecture Model with Service Composition focus*

Figure 4.3 depicts the need for a service discovery service that can search, match and select from the Gaia-X service catalog. The search for matching services may concern other service catalogs and offers depending on the will of the users to combine service offerings from different sources with verified interoperability and compatibility characteristics available in the catalog itself. Note that the discovery can be basic and limited to consumers selecting services by themselves or more elaborate via intelligent service discovery based on demands expressed using natural language or intent based paradigms. Such service discovery services as well as composition and orchestration engines are out of the scope of Gaia-X but found in the Gaia-X federated services catalog offerings proposed by providers, digital clearinghouses and third parties.

End users or tenants express theis initial service request and set both their requirements and constraints in their demand.

![Detailed Service Composition Model](figures/Fig_4.3_service_composition_rev.drawio.png)
*Figure 4.3 - Generic and abstract conceptual service composition model*

The service composition module first “analyzes, parses and decomposes” the service request.
It then prepares a set of service discovery queries to find and select the candidate services among the service offerings via the Federation Services.

Once the user has negotiated and approved the selected services and providers, candidate services are returned to service composition in order to build service deployment and instantiation workflows and plans.
A contract is also established accordingly.
The service composition module realizes the functional version of the solution and initiates service binding with the providers’ provisioned resources.

Starting from this abstract implementation plan for the Gaia-X Services Composition process, real implementations by suppliers will be developed.
The actual deployment, binding and connectivity occur via an orchestrator, responsible for the coordination and organization of service instances, as well as the initial deployment, configuration and activation of such created service instances.

The orchestrator interacts with deployment and configuration tools ensuring the actual configuration, control and dynamic adaptation of service instances.
A service life cycle manager is also associated with the orchestrator to manage services at run time.
The orchestrator and service life cycle manager are typically external and act as the interface between service composition plans and providers.
Optionally, the consumer acting as a broker can realize service composition.
In this case, the consumer actively drives the orchestrators and partakes in the orchestration and service life cycle management according to compatible service deployment and management tools specified by the providers.

These tools and management systems can be proprietary or readily available in Catalogues.
Popular configuration and deployment tools and systems such as Terraform, Ansible, Heat and Salt Stack are examples used for deployment and configuration purposes in cloud infrastructures.
If service instances and resources are hosted in containers rather than in virtual machines, an intermediate container orchestration and management system such as Kubernetes is then used in an intermediate step, which is out of the scope of this document.

## Identity and Access Management

The identities of participants in the Gaia-X Ecosystem rely on signed attributes, which can be requested and exchanged to gain individual trust from other participants.

Each Participant provides a unique identifier that is associated with the attributes and the Participant can cryptographically prove that this Identifier is under his control.

The attributes are evaluated by the Participants to implement, enforce, and monitor permissions, prohibitions and duties when negotiating a contract for services or resources.

In the context of Gaia-X, the attributes are encapsulated in the [**Party Credential**](https://gaia-x.gitlab.io/technical-committee/federation-services/icam/party_credential/) associated with its controlled identifier.
Examples of [**Party Credential**](https://gaia-x.gitlab.io/technical-committee/federation-services/icam/party_credential/) specializations are:

- Natural Person Party Credential 
- Legal Person Party Credential
- Service Party Credential
- Membership Party Credential
- Custom Party Credential (domain specific party credential)

 Another important type of credential is represented by the **TrustAnchorCredential** that provides a semantic description, the scope, the issuer(trust anchor), and any additional needed information by the **PartyCredential**.
 The **TrustAnchorCredential** mainly defines:

- The **Scope** where the **PartyCredential**s are considered valid.  
- The **TrustedIssuer** entitled of issuance of the **PartyCredential** in the above **Scope**
- The **Vocabularies** (expressed in SHACL) that semantically defines the **PartyCredential** extended properties and the embedded attributes 
- The **Trusted List** of issued **PartyCredential** in the scope by the trusted issuer.

Like for the **PartyCredential**, there are also several specializations of the **TrustAnchorCredential**, here are some examples:

- **Organization Trust Anchor** – a self issued credential that entitles an organization to define:
  - **scope** - *Organization Credential Management* (OCM).
  - **trusted issuer** - itself.
  - **vocabularies** - defines the semantics of *Roles/Identity Attributes* and **Domain Specific Credentials** valid in the defined scope.
  - **trusted list** - to assign and revoke *Roles/Identity Attributes* to its parties (users, natural persons, endpoint services, etc) issuing **PartyCredential** (referencing vocabularies and embedding the above *Roles/Identity Attributes*)
- **Ecosystem Trust Anchor** - a self issued credential that entitles an Ecosystem operator to define:
  - **scope** - manage an *Ecosystem* (onboarding/offboarding/role assignement etc.).
  - **trusted issuer** - itself.
  - **vocabularies** - defines the semantic of *Roles/Identity Attributes* and **Domain Specific Credentials** valid in the defined scope.
  - **trusted list** - to assign and revoke *Roles/Identity Attributes* to its members (other Participants) issuing **MembershipPartyCredential** (referencing vocabularies and embedding the above *Roles/Identity Attributes*)
- **Labelling Trust Anchor** - a Gaia-X issued credential that entitles a Participant to act as a CAB entitled to issue attestations related to services conformity labels.
  - **scope** - issue Gaia-X conformity labels .
  - **trusted issuer** - the Participant selected by Gaia-X to operate as CAB.
  - **vocabularies** - defines the semantic of *Label*s that are valid in the defined scope.
  - **trusted list** - to assign and revoke *Labels* issued to other Participants


!!! note

    The reader must strongly distinguish between identifiers and identities.
    An identifier is a unique attribute that is part of the Participant's identity.

The key elements of the trustworthiness of an identity at the time of the negotiation are:

- The demonstration that the participant identifier is under the control of the credential holder using the verification method bound to the credential(**holder** property of [**Party Credential**](https://gaia-x.gitlab.io/technical-committee/federation-services/icam/party_credential/)).
- The issued identity attributes conform with the [KYB/KYC](https://en.wikipedia.org/wiki/Know_your_customer) rules associated with the issuance of the identifier.
- The identifier issuers are trustworthy parties by checking the Gaia-X Registry and optionally other [Verifiable Data Registries](https://www.w3.org/TR/vc-data-model/#dfn-verifiable-data-registries) extending the Gaia-X rules.
- The check of **credentialStatus** property to be not equal to **revoked/suspended** (see [Party Credential Status](https://gaia-x.gitlab.io/technical-committee/federation-services/icam/party_credential/) and [ W3C Verifiable Credentials Bitstring Status List v1.0](https://www.w3.org/TR/vc-bitstring-status-list/) for additional information)

!!! note

    [did:web](https://w3c-ccg.github.io/did-method-web/) requires the control of the DNS or web service resolving the DID identifier.
    However, even with DNSSEC and an EV SSL cert, a did:web identifier offers a low level of confidence for legal participant authentication.
    For this reason, the following additional constraint would help to raise the level of confidence:
    Considering the key pair used by the legal participant to onboard to Gaia-X (to sign his own Participant Credential) as **the only one** valid to be used to sign/issue subsequent credentials, will ensure that only the holder of the private key associated with the key pair is trustworthy, making any kind of did:web hacking and/or DNS attack completely useless.
    
### Dataspace / Federation onboarding and offboarding

The onboarding and offboarding of entities - participants, services, resources - within a Data Space or Federation is under the responsibility of the Data Space and Federation authorities which are autonomous from Gaia-X.

The above onboarding and offboarding processes are convenient optional ways for the participants themselves to request, demonstrate, collect, and revoke attributes/properties about themselves, their services, and their resources.

Those attributes/properties are expressed and exchanged via the [**Membership Party Credential**](https://gaia-x.gitlab.io/technical-committee/federation-services/icam/party_credential/) specialization.

Gaia-X does not mandate nor enforce a particular timeline to gather Gaia-X Credentials.

Those could also be gathered at the time of contract negotiation, on the fly, depending on the policies being negotiated.

#### Layered identity and access management

The identity and access management relies on a two-tiered approach.
In practice, this means that participants use a few selected identity systems for mutual identification and trust establishment, SSI being the recommended option for interoperability.
After trust has been established, underlying existing technologies already in use by participants can be federated and reused, for example Open ID Connect (using [OpenID4VP](https://openid.net/specs/openid-connect-4-verifiable-presentations-1_0-07.html) and [SIOPv2](https://openid.net/specs/openid-connect-self-issued-v2-1_0.html)) or domain-specific X.509-based communication protocols.

Gaia-X participants might need to comply with additional requirements on the type and usage of credentials management applications, such as mandatory minimum-security requirements, like multi-factor authentication.
Server-to-server communication plays a crucial role in Gaia-X.

#### Architecture principles for this approach

Mutual trust based on mutually verifiable Participant identities between contracting parties, Provider and Consumer, is fundamental to federating trust and identities in the End User (PartyCredential holder) layer.
Heterogeneous ecosystems across multiple identity networks in the participant layer must be supported as well as heterogeneous environments implementing multiple identity system standards.
The high degree of standardization of participant building blocks provided by the mandatory attributes defined by Gaia-X must ensure that there is no lock-in to any implementation of identity networks and identity systems.

#### Chain of trust and identity

In the participant layer, the Gaia-X specifications indicate how to resolve and verify the participant identity of the contracting parties.
The Consumer verifies the Provider's identity, the Provider verifies the Consumer's identity.
Successful mutual participant verification results in a verified participant Token representing the trust between Provider and Consumer.
The token payload contains one or more [Gaia-X Credentials](operating_model.md#gaia-x-credentials-and-attestations).

## Data Products and Data Exchange Services

Enabling digital transformation as well as developing innovative services requires the right data at the right time, aggregating from multiple sources to produce valuable insightful information. 
However, within most organizations, data sharing is too often stalled by stakeholder resistance, data governance policies, lack of tools, and inability to address regulatory restraints:
-	For a data producer, personal and non-personal data sharing is often associated with a legal risk (e.g. personal data, as per GDPR, shared without the person’s consent), with an industrial risk (e.g. data including some intellectual property or commercial secret communicated to competitors) or with a reputation and image risk (e.g. data privacy shaming if shared data is mis-used), while there are barely any established local benefits for sharing data.
-	For a data consumer, appropriate use and corresponding usage restrictions are rarely clearly, nor formally, stated or are expressed in a legal form which is cumbersome to check and is difficult to enforce automatically.
-	For an entity’s legal and compliance structures, data access and governance processes are often fragmented, and verifications of appropriate data usage are complex and labor intensive (i.e. checking that received data are used in a not-illegal way is complex).
This complexity often results in decisions that are overly risk averse, blocking data sharing with a “stop first and thoroughly assess” mechanism, preventing business opportunities from driving digital transformation and from grabbing competitive advantages.

Overcoming these resistances requires to establish reliable trust mechanisms throughout the data sharing process.
First, the original data rights holder need to be able to express how their data shall be used, and they need confidence that these constraints will be duly applied.
Then, the data consumers need evidence that the data is genuine, and that the usage authorization is legitimate (i.e. that they are not illegally using the data).
Respectively, the data providers need evidence that the data consumers have the authorization to receive the data (i.e. that providing the data is not illegal).

The Gaia-X Data Product concept and its operational model provide such mechanisms. In addition, they provide mechanisms to facilitate and demonstrate compliance with the European regulations regarding data (GDPR and Data Act).

### Data Product Conceptual Model

![Data Product Conceptual Model](figures/Gaia-X_models_Data_Product_Conceptual.svg)

*Figure 4.4 - Data Product conceptual model*

Data are furnished by `Data Producers` to `Data Providers` who compose them into a `Data Product` to be used by `Data Consumers`. Data owners and data controllers in the GDPR sense, or data holders in the Data Act sense are Data Producers – other kinds of Data Producers can be defined by different ecosystems.

A Data Products is described by a `Data Product Description`, which must be a valid Gaia-X Credential and is stored in a (searchable) `Federated Data Catalogue`.

Data Product Descriptions contain the `Metadata` describing the data (scope, format, quality, etc.) using an ontology which is defined by the ecosystem and contains information describing the contractual and operational aspects of the Service Offering (cost and billing, technical means, service level agreement, etc.).


Before using a Data Product, the Data Consumer negotiates and co-signs a `Data Usage Contract` (DUC) with the Data Provider.
This Data Usage Contract is based on the Data Product Description and includes the service configuration element and mutually agreed and enforceable `Terms of Usage`, resulting from potential negotiations. Hence a Data Product Description constitutes a Data Usage Contract template.

The Data Product Usage Contract is a Ricardian contract: a contract at law that is both human-readable and machine-readable, cryptographically signed and rendered tamper-proof, and electronically linked to the subject of the contract, i.e., the data.
The parties can (optionally) request this contract to be notarized in a federated `Data Usage Contract Store`.

!!! NOTE: A Data Usage Contract is often organized in several parts: (a) an ecosystem-level contract agreed/signed by all participants of the ecosystem (sometime called ecosystem policy scheme), (b) a frame contract between the Provider and the Consumer defining the overall terms and conditions governing the contractual relationship for a set of services and (c) an application contract specific to the ordered service. 

After such a contract has been agreed upon and has been signed by both parties, the Data Consumer can start accessing the data (`Data Access`) and then using the data (`Data Usage`), realizing the Data Product Usage Contract.
The contract negotiation can lead to both parties agreeing on a `Data Usage Logging Service` (these logs might also include information needed for billing, inc. service level details, even if billing is outside Gaia-X perimeter).

If a specific license is attached to some data in the Data Product, then the Data Product Description shall contain a `Data License` defining the usage policies for the data and, before Data Usage, the Data License shall be derived into a `Data Usage Agreement` (DUA) signed by the `Data Rights Holder` and by the Data Consumer. Data Usage Agreements are notarized by a `Data Usage Agreement Trust Anchor` (DUA Trust Anchor) and can be revoked at any time by the Data Rights Holder.

The signed Data Usage Agreement is communicated to the Data Provider, who must check its validity through the DUA Trust Anchor before each Data Usage delivery (i.e. each time the data usage is requested by a Data Consumer, especially for recurrent data usages).

This signed Data Usage Agreement gives (a) to the Data Consumer the legal authorization to use the data in accordance with the constraints specified by the Data Rights Holder and (b) gives to the Data Rights Holder the assurance that the Data Consumer commits to respect these constraints. 

!!! NOTE: this signature can be a digital signature (as for instance an eIDAS signature) or simply an electronic form (as a click on a “I agree” button in a specific screen provided by the DUA Trust Anchor). 

The Data Usage Agreement concept is a general concept which addresses every kind of licensed data and hence encompasses also the concepts of _Consent_ from GDPR and of _Permission_ from the EU Data Act.
In case of data liable to legal regulation (e.g. GDPR or Data Act), the Data Usage Agreement must contain all information required by the regulation (in particular the purpose of usage).

If the Data Product contains data from several Data Rights Holders, then a Data Usage Agreement shall be signed by each Data Rights Holder and all these signed Data Usage Agreements shall be communicated to the Data Provider before Data Usage.

### Data License and Data Usage Agreement

Data Usage Agreements enable Data Rights Holders to control how their data are used and by whom (this is called _data sovereignty_).

Data Licenses contain a set of constraints related to the authorized or forbidden usage of the data in the Data Product. Data Usage Agreements are usually derived from the Data License but might differ according to the result of the negociation between the Data Right Holder and the Data Consumer. Data Usage Agreements also include additional information related to the identity of the Data Consumer, the detailed purpose of the data usage, the duration of the agreement, etc. 

Data Usage Agreements contains two sets of constraints: the _Data Access Prerequisites_, which are enforced by the Data Provider before delivering access to the data, and the _Data Usage Constraints_, which are outside the scope of the Data Provider and shall be respected by the Data Consumer when using the data. For instance, restricting data access to research laboratories with a specific ISO certificate can be enforced by the Data Provider while restricting data usage to research related to a specific disease is enforceable only by the data consumer.

To enable automated processing, Gaia-X mandate the use of Open Digital Rights Language (ODRL) from W3C to express Data License constraints (cf. https://www.w3.org/TR/odrl-model/) – using an ontology which will usually be defined by the ecosystem.

A data License can be generic, for instance “I agree that my data are used by any non-profit licensed health laboratory with XYZ security level” or specific “My data can be used only with a specific DUA signed by me and including the identity of the data consumer and the explicit consent of the data usage."

In the first case (generic license), a DUA signed by the Data Rights Holder is pre-notarized and there is no need for further communication with the Data Rights Holder before Data Usage: the DUA identifier can be stored in the Data Product Description and the Data Consumer just needs to sign it, notarize it and communicate the identifier to the Data Provider.
In the second case (specific license), the Data Rights Holder must be contacted (either by the Data Consumer or by the Data Provider) in order to fill the DUA and sign it.
More details are provided in the Operational Models section.

### DUA verification process

The actors involved in a specific Data Usage must check:
1.	The DUA validity: is the entity signing the DUA really the Data Rights Holder (or a legal delegate of the Data Rights Holder)? 
2.	The DUA applicability: is the Data Consumer entitled to access the data (i.e. it fulfils the _Data Access Prerequisites_ conditions expressed in the Agreement)?
3.	The DUA status: is the DUA active (i.e. not expired and not revoked)?

To enable the DUA validity check, the DUA shall include Verifiable Credentials “proving” that the Data Rights Holder is legally entitled to sign the DUA (i.e. to authorize data usage). Such credentials might depend on business/legal logic and trusted sources specific to the business domain. For instance, a farmer can agree to share data related to a parcel only if it is a farmer and if it owns or rents the parcel.

To enable DUA applicability check, the Data Consumer must provide for each ORDL rule in the DUA, the verifiable credentials “proving” that it fulfils the constraints of the rule. To facilitate applicability check, it is recommended to specify in the Data Product Description the list of accepted Verifiable Credentials and the accepted issuers.

Data Providers must check the DUA validity, the DUA applicability and the DUA status before each Data Usage. 
Data Consumers must check the DUA validity and the DUA status before using the data.

As such checks can require domain-specific logic and hence can be complex to implement, it is likely that most ecosystems will mandate DUA Trust Anchors to provide services to check DUA validity and DUA applicability (hence acting as policy information points), in addition to basic notarization services.


### Cascading agreement and right to oblivion

The above conceptual model can be applied recursively: the Data Consumer can integrate the Data into a new Data Product that can be used by other Data Consumers, who can in turn create new Data Products.
It provides convenient mechanisms for Data Rights Holders to control who is using their data and to revoke usage agreements.

Indeed, the above model blocks subsequent data transmissions between the Data  Provider and the Data Consumer when the Data Usage Agreement is revoked and hence it provides a more robust mechanism than the usual cascading mechanism where the chain of revocations will be broken if one of the participants in the usage chain is defective.

In order to better support the right to oblivion, it is recommended that the Data Space defines a general policy mandating each participant to check the Data Usage Agreement validity before reusing the data (even internally, when they don’t request new Data Usage from the Data Provider).
How a participant implements this policy depends on its own internal data management procedures and is outside the scope of Gaia-X.

### Data Intermediaries

_Data intermediary_ services are explicitly mentioned as an important concept in European Commission acts around data.
Their definition is quite broad: “Data intermediation services may support users or third parties in establishing a commercial relation for any lawful purpose on the basis of data of products in the scope of this Regulation e.g., by acting on behalf of a user” (cf. Regulation (EU) 2022/868).

Compared to the general Gaia-X Conceptual Model, a Data Intermediary is an actor who combines several roles: Data Provider acting as a proxy between the Data Producer and the Data Consumer, Federator operating a Data Product Catalogue (often named data marketplace), Data Usage Agreement Trust Anchor acting as a trusted proxy between the Data Rights Holder and the Data Consumer, etc.

Several [Gaia-X Lighthouse projects](https://gaia-x.eu/who-we-are/lighthouse-projects/) are implementing this concept of Data Intermediary, with different perimeters and operating models adapted to their ecosystem specificities.
Accordingly, it was decided to stick here to basic data exchange concepts and to let Lighthouse projects define higher level concepts like Data Intermediary according to their needs.


## Gaia-X Trust Anchors

Gaia-X Trust Anchors are bodies, parties, i.e., Conformity Assessment Bodies or technical means [accredited](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.7) by the Gaia-X Association to be trustworthy anchors in the cryptographic chain of keypairs used to digitally sign statements or claims about an object.

For each accredited Trust Anchor, a specific [scope of attestation](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.4) is defined.

The list of valid Trust Anchors and rules is available via the Gaia-X Registry.

There are cases where the Trust Anchor is relative to another property in a claim.

Example: In the case of a `gx:DataResource`, the `consent` [property](https://www.w3.org/TR/vc-data-model/#dfn-property) must be signed by the `gx:Participant` identified by the `dataController` property, itself signed by the `gx:Participant` identified by the `producedBy` property, itself signed by the credential's `issuer` with a keypair from a chain of certificates with at its root a Gaia-X accredited state [Trust Service Provider](https://portal.etsi.org/TB-SiteMap/ESI/Trust-Service-Providers).

Ecosystem Trust Anchors follow the same principle, but are defined by the ecosystem-specific governance.

```mermaid
---
title: example of multiple Trust Anchors for a Resource class 
---

erDiagram
    gx:DataResource {
        ODRL consent "claim signed by the dataController"
        DID dataController "claim signed by the producedBy"
        DID producedBy "claim signed by the issuer of the credential"
    }
```

In this example, the `dataController`'s participant, `producedBy`'s participant and the Trust Service Provider are Trust Anchors because they are mandatory fixed anchors in the credential chain of signatures.
In this same example, all the claims could also be signed by the `issuer` if the `dataController`'s participant, `producedBy`'s participant and `issuer` are the same participant.

### Gaia-X Trusted Data sources and Gaia-X Notaries

When an accredited Gaia-X Trust Anchor is not capable of issuing cryptographic material nor signing claims directly, then the Gaia-X Association accredits one or more Notaries, which will perform a [validation](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:6.5) based on objective evidence from a Gaia-X Trusted Data source and will issue an [attestation](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.5) about the previously made attestation from the Trust Anchors.

The Notaries are not performing [audit](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:6.4) nor [verification](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:6.6) of the [object of conformity](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.2).

The Notaries are converting "not machine readable" proofs into "machine-readable" proofs.

<table>
<thead>
  <tr>
    <th>ISO/IEC 17000:2020</th>
    <th>Not machine readable</th>
    <th>Machine readable</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td><a href="https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.3">first-party conformity assessment activity</a></td>
    <td rowspan="3">signing a paper, a PDF, a doc, …</td>
    <td rowspan="3">signing a verifiable credential, a PDF/A-3a, …</td>
  </tr>
  <tr>
    <td><a href="https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.4">second-party conformity assessment activity</a></td>
  </tr>
  <tr>
    <td><a href="https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.5">third-party conformity assessment activity</a></td>
  </tr>
</tbody>
</table>

Example: The European Commission provides several APIs, including one to check the [validity of EORI number](https://ec.europa.eu/taxation_customs/dds2/eos/eori_validation.jsp).
Unfortunately, those APIs are not returning Verifiable Credentials.
Hence Gaia-X accredited the [GXDCH](https://gaia-x.eu/gxdch/) to act as a Notary for EORI verification using the European Commission API as the Gaia-X Trusted Data source for EORI validation.

Example: For a CAB or a set of CABs performing third-party conformity assessment activities but not capable of digitally signing the attestations, the Gaia-X Association could accredit one or more Notaries.
The role of those Notaries is to digitalize an assessment previously made.
The Notaries are not performing the assessment.
The **Verifiable Credential** signed by such a Notary will be a second-party conformity assessment activity and have as `credentialSubject` a statement of a third-party conformity assessment activity.
The overall trust level of such a **Verifiable Credential** will be lower than if the CAB was able to digitally sign the **Verifiable Credential** directly without involving a Notary.

#### Evidence

It is expected that the credentials issued by the Notaries contain the [evidence](https://www.w3.org/TR/vc-data-model-2.0/#evidence) of the validation process.

The Gaia-X Trust Framework provides the definitions and means to manage Gaia-X Conformity, Gaia-X Labels and Ecosystem Trust.
The Architecture Document describes the technical means to digitally notarize claims by a Gaia-X participant based on the W3C Verifiable Credentials Data Model Recommendation.