# Enabling and Federation Services

Enabling Services facilitate the operation of ecosystems. There are multiple technologies, products and implementations of each of the enabling services available. This document defines the components, the interactions among them and specific requirements towards Gaia-X.

Enabling services operated under the specific rule of ecosystem governance are defined as "Federation Services".

## Wizard Service

UI to sign Verifiable Credentials in a Verifiable Presentation on the client side.
Calling the Gaia-X Compliance Service is integrated with the Wizard, making it possible to obtain Gaia-X Compliant Credentials by directly using this tool.
Depending on the implementation, a Wizard can support signing with a hardware module via [PKCS#11](https://en.wikipedia.org/wiki/PKCS_11) or other like [Web eID](https://web-eid.eu/).

## Credential Manager

<!-- langec: partly redundant with digital_clearing_house.md -->
Application that allows the user to store their Credentials and present them to third parties when needed.
This is also sometimes called an Agent, CredentialRegistry or [IdentityHub](https://identity.foundation/decentralized-web-node/spec/0.0.1-predraft/).

## Federated Catalogues

The goal of the Federated Catalogue is to:

- enable Consumers to find best-matching offerings and to monitor them for relevant changes in the offerings
- enable Producers to promote their offerings while keeping full control of the level of visibility and privacy of their offerings.
- enable Service Composition by including and publishing Service Descriptions, conformant to the Gaia-X Schema, that contain structured service attributes required to compose services.
- avoid a gravity effect with a lock-out and lock-in effect around a handful of catalogue instances.

### Catalogue service

The implementation of the catalogue can vary from one catalogue owner to another and offers different user experiences.

The requirement to be listed in the Gaia-X Registry as a valid Gaia-X Catalogue is to keep the network of Gaia-X Catalogues up to date by publishing in the pubsub service the credential ID of the credential being created, updated or revoked.

A future testbed will be implemented to perform dynamic validation of the above behavior.

### Retention

To have a basic data curation mechanism, the retention period in the pubsub service is one-year maximum.

It means that a holder, to have their credentials discoverable, should publish them at least once per year, even if the credentials have not changed.

### Snapshot

To speed up the onboarding of new catalogue instances, it is expected that timestamped snapshots of the pubsub service data are made and stored publicly via [IPFS](https://ipfs.tech/).
The URIs of the snapshots are available via the Gaia-X Registry.

### Trust Indexes

While "Trust" is used as a building block in every Data Space and Federation related document, there are two main challenges:

- There is no unique definition of Trust; Trust is context-dependent
- The market has and will always move faster than the rules

While the [Gaia-X Policy Rules](https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/) provide a baseline translating European values into objectives and measurable criteria, Data Spaces and Federations need to [extend this baseline](operating_model.md#extention-by-the-ecosystems) for their own market, vertical domain and local regulations.

In this context, the Trust Indexes are a means to measure trust interoperability across dataspaces and federations adopting and extending the Gaia-X Policy Rules.

Four indexes are proposed:

- Veracity
- Transparency
- Composability
- Semantic Match

#### Veracity

The Veracity index is a function of:

- for a given claim, the length of the signing key chain (root → intermediate → intermediate → … → leaf)

```mermaid
flowchart LR
    keychain1[root]
    keychain2[intermediate]
    keychain3[...]
    keychain4[leaf]

    keychain1 --> keychain2 --> keychain3 --> keychain4
```

- for a given claim, the number of signatures on that claim (root → … → leaf ← … ← root)

```mermaid
flowchart LR
    keychain1[root #1]
    keychain2[intermediate #1]
    keychain3[root #2]
    keychain4[intermediate #2]
    keychain5[leaf]

    keychain1 --> keychain2 --> keychain5
    keychain3 --> keychain4 --> keychain5
```

#### Transparency

The Transparency index is a function of:

- the number of exposed optional properties of an object versus mandatory - in terms of the Gaia-X Policy Rules - properties of the same object. This ratio is always greater or equal to 1.<!-- langec: but only if you define it as "# overall properties / # mandatory properties" -->
- the shape of the graph formed by the linked claims, measuring its eccentricity and depth.

#### Composability

The Composability index is a function of two or more service descriptions, computing:

- the capacity of those services to be technically composed together, e.g.: software stack, compute, network and storage characteristics, plugin, and extension configurations, …

This index is computed, for example, by instances of the Federated Catalogues, by analysing and comparing the characteristics of several service descriptions.

This index can be decomposed for each service description into several sub-functions:

- the level of detail of the [Software Bill of Material](https://www.cisa.gov/sbom)
- the list of known vulnerabilities such as the ones listed on the [National Vulnerability Database](https://nvd.nist.gov/).
- Intellectual Property rights via the analysis of the licenses and copyrights.
- the level of stickiness or adherence to specific known closed services
- the level of readiness for lift-and-shift migration.

#### Semantic Match

The Semantic Match index is a function of:

- the use of recommended vocabularies (Data Privacy Vocabulary, ODRL, …)
- the unsupervised classification of objects based on their properties
- Natural Language Processing and Large Language Model analysis from and to Domain Specific Language (DSL)
- Structured metadata information embedded in unstructured data containers (PDF/A-3a, …)

## Notarization service(s)

Notarization services are built to support the validation claims to verifiable credentials by allowing to review attestations by "Trusted Data Sources" and issuance of a verifiable credential once all conditions of the policies are met (e.g. the compliance service of the GXDCH uses the "Gaia-X registryNumber notarization API" to validate the company registration number).

A more generic set of services is provided by the Eclipse XFSC project which provides components to define a complex workflow and manage the chain of attribute validations and issuance of the VC through the NOT, TSA and WFE components.

## Data Services

Data access and data usage in Gaia-X is enabled by a set of services that are used by the involved actors and are supported as Federation Services. Some of these services are specific to data access/usage or include data related specificities - these are referred as Data Services. They are listed below and fully described in the Data Services specifications document.
Not all Data Services are mandatory.


1. **Data Product Catalogue Services** (mandatory) provide mechanisms to publish Data Product Descriptions (inc. metadata) and to support search. Data Product Catalogue Servicess are specific in the sense that they use DCAT as core protocol for Data Product description. Gaia-X provides an initial set of **Vocabularies** to describe Data Product – Gaia-X Data Product Catalogues must be extensible with vocabularies from different (business or technical) domains.

2. **Data Usage Agreement Services** (mandatory) provide mechanisms to notarize/revoke Data Usage Agreements (DUA), to check their status, their validity (i.e. check that the signatoree is really holding rights on the data) and their applicabilibility (i.e. that the Data Access Pre-requisites are fullfilled).

3. **Data Access Protocols** are required to exchange data between Participants and hence to enable Data Access. Data exchanges are realized on a peer-to-peer basis. Gaia-X does not promote any technical protocol - the actual protocol must be agreed between the parties during the contracting phase.

4. **Data Access Logging Services** (optional) provide evidence that data has been (a) actually accessed (i.e. provided and received) and (b) that Data Usage Agreement was enforced before access. Data Providers can use these services to demonstrate to Data Right Holders (and to the Dataspace Authority) that all data accesses were performed according to their requirements.  Additionally Data Consumers can use these services in case of SLA dispute.

### Non-specific services

Other Federation Services are needed to realize the complete life cycle of Data Access / Data Usage but don't carry spcificities related to data:
1. **Authentication and Digital Signature Services** (mandatory) are essential to identify participants and to record trustworthy data usage agreements. They are provided acording to the specifications defined in the _Gaia-X Identity, Credential and Access Management_ document.
2. **Negotiation and Contracting Services** (mandatory) enable parties to negotiate Service Contracts, including Data Usage Contracts. 
3. **Contract Store Services** (optional) enable parties to notarize a contract (i.e. the result of the negotiation phase).

These services are not decribed further here.

